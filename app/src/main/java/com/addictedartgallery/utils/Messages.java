package com.addictedartgallery.utils;


import android.support.design.widget.Snackbar;
import android.view.View;

public class Messages {


    public static void show(View view, int code) {
        String message;


        switch (code) {
            case 1:
                message = "Please check your network connection";
                break;

            case 2:
                message = "Please Enter Valid Credentials";
                break;

            case 3:
                message = "Something went wrong. Please try again later.";
                break;

            default:
                message = "Network isn't available";
        }


        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }

    public static void show(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }


}
