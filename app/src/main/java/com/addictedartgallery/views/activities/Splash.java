package com.addictedartgallery.views.activities;


import android.os.Bundle;
import android.support.annotation.Nullable;

import com.addictedartgallery.Dashboard;
import com.addictedartgallery.R;
import com.addictedartgallery.model.AuthenticateGetUser;
import com.addictedartgallery.model.AuthenticatePostUser;
import com.addictedartgallery.model.Data;
import com.addictedartgallery.model.PostData;
import com.addictedartgallery.rest.ApiClient;
import com.addictedartgallery.rest.ApiInterface;
import com.addictedartgallery.utils.ThreadExecution;
import com.addictedartgallery.utils.ViewUtils;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Splash extends BaseActivity {


    AVLoadingIndicatorView progressBar;
    ApiInterface apiService;
    AuthenticateGetUser authenticateGetUser;
    AuthenticatePostUser authenticatePostUser;
    PostData postData;
    Data data;
    String encodedEmail, messageDigest1, messageDigest2, messageDigest;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        progressBar = (AVLoadingIndicatorView) findViewById(R.id.splash_screen_progress_bar);
        int currentDate = (int) (new Date().getTime() / 1000);

        if (expire != null) {
            int expireDate = Integer.parseInt(expire);
            if (currentDate >= expireDate) {
                if (password != null) {
                    apiService = ApiClient.getClient().create(ApiInterface.class);
                    Call<AuthenticateGetUser> call = apiService.getAuthToken();
                    call.enqueue(authenticateGetUserCallback);
                } else
                    ThreadExecution.startThread(this, Login.class);
            } else
                ThreadExecution.startThread(this, Dashboard.class);
        } else
            ThreadExecution.startThread(this, Guide.class);


    }


    private Callback<AuthenticateGetUser> authenticateGetUserCallback = new Callback<AuthenticateGetUser>() {
        @Override
        public void onResponse(Call<AuthenticateGetUser> call, Response<AuthenticateGetUser> response) {

            if (response.isSuccessful()) {
                authenticateGetUser = response.body();
                if (authenticateGetUser.getStatus()) {
                    data = authenticateGetUser.getData();
                    try {
                        encodedEmail = URLEncoder.encode(email, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        encodedEmail = null;
                    }
                    messageDigest1 = ViewUtils.getMd5Key(email + ":" + data.getRealm() + ":" + password);
                    messageDigest2 = ViewUtils.getMd5Key("POST:"+getString(R.string.PostUrl)+":access_expire=36000&auth_request=" + data.getAuthRequest() + "&expire=" + data.getExpire() + "&username=" + encodedEmail);
                    messageDigest = ViewUtils.getMd5Key(messageDigest1 + ":" + data.getAuthRequest() + ":" + messageDigest2);

                    Call<AuthenticatePostUser> postCall = apiService.getAccessToken("36000", data.getAuthRequest(), String.valueOf(data.getExpire()), email, messageDigest);
                    postCall.enqueue(authenticatePostUserCallback);

                } else
                    ThreadExecution.startScreen(Splash.this, Login.class);


            } else
                ThreadExecution.startScreen(Splash.this, Login.class);
        }

        @Override
        public void onFailure(Call<AuthenticateGetUser> call, Throwable t) {
            ThreadExecution.startScreen(Splash.this, Login.class);
        }
    };


    private Callback<AuthenticatePostUser> authenticatePostUserCallback = new Callback<AuthenticatePostUser>() {
        @Override
        public void onResponse(Call<AuthenticatePostUser> call, Response<AuthenticatePostUser> response) {
            if (response.isSuccessful()) {
                authenticatePostUser = response.body();
                postData = authenticatePostUser.getData();

                if (authenticatePostUser.getStatus()) {
                    preferences.setUserDetails(postData.getAuthRequest(), String.valueOf(postData.getExpire()), email, password, postData.getRealm());
                    ThreadExecution.startScreen(Splash.this, Dashboard.class);
                } else
                    ThreadExecution.startScreen(Splash.this, Login.class);

            } else
                ThreadExecution.startScreen(Splash.this, Login.class);

        }

        @Override
        public void onFailure(Call<AuthenticatePostUser> call, Throwable t) {
            ThreadExecution.startScreen(Splash.this, Login.class);
        }
    };
}
