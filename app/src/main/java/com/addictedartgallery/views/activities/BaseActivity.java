package com.addictedartgallery.views.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.addictedartgallery.utils.Preferences;
import com.addictedartgallery.utils.ViewUtils;


public abstract class BaseActivity extends AppCompatActivity{

    protected Preferences preferences;
    protected String expire, password, email;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        ViewUtils.cleanDirectory(this);
        preferences = new Preferences(this);

        expire = preferences.getExpire();
        password = preferences.getPassword();
        email = preferences.getUsername();


    }
}
