package com.addictedartgallery.widgets.dialogs;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.addictedartgallery.R;
import com.addictedartgallery.utils.Preferences;
import com.addictedartgallery.utils.ThreadExecution;
import com.addictedartgallery.views.activities.Login;

public class LogOut {

    public static void show(final Activity activity, final Preferences preferences)
    {
        final Dialog logOutDialog = new Dialog(activity);
        logOutDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        logOutDialog.setCancelable(true);
        logOutDialog.setContentView(R.layout.dialog_logout);

        Button logOutButton = (Button)logOutDialog.findViewById(R.id.dialog_logout_logoutButton);
        logOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                preferences.clearData();
                logOutDialog.cancel();
                ThreadExecution.startScreen(activity, Login.class);
            }
        });

        Button backButton = (Button)logOutDialog.findViewById(R.id.dialog_logout_backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logOutDialog.cancel();
            }
        });

        logOutDialog.show();

    }
}
