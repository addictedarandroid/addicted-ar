package com.addictedartgallery.widgets.progressbar;

interface Determinate {
    void setMax(int max);
    void setProgress(int progress);
}