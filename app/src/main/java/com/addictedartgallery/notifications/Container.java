package com.addictedartgallery.notifications;


import com.addictedartgallery.model.ProfileData;
import com.addictedartgallery.utils.Preferences;

public class Container {

    public ProfileData profileData;
    public Preferences preferences;
    public String token;

}
