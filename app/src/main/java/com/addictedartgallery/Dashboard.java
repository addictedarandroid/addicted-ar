package com.addictedartgallery;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import com.addictedartgallery.model.Profile;
import com.addictedartgallery.notifications.Container;
import com.addictedartgallery.notifications.SNSPush;
import com.addictedartgallery.utils.Messages;
import com.addictedartgallery.utils.ThreadExecution;
import com.addictedartgallery.utils.ViewUtils;
import com.addictedartgallery.views.activities.Guide;
import com.addictedartgallery.views.activities.Login;
import com.addictedartgallery.widgets.dialogs.ArtWork;
import com.addictedartgallery.widgets.dialogs.Feedback;
import com.addictedartgallery.widgets.dialogs.LogOut;
import com.addictedartgallery.widgets.dialogs.NetworkError;
import com.addictedartgallery.widgets.dialogs.NoArtWork;
import com.addictedartgallery.widgets.dialogs.NotificationDialog;
import com.addictedartgallery.widgets.tooltips.Tooltip;
import com.addictedartgallery.widgets.tooltips.TooltipAnimation;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import eu.kudan.kudan.ARImageNode;
import eu.kudan.kudan.ARImageTrackable;
import eu.kudan.kudan.ARImageTrackableListener;
import eu.kudan.kudan.ARNode;
import eu.kudan.kudan.ARTexture2D;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Dashboard extends BaseDashboardActivity implements View.OnClickListener, ARImageTrackableListener, AdapterView.OnItemClickListener {

    AVLoadingIndicatorView progressBar;
    private FloatingActionButton nextButton, previousButton;
    private FloatingActionButton shopIcon, infoIcon, hamIcon;
    private Button betaButton;
    private ViewGroup root;
    private ImageView logo;


    Call<Profile> profileCall;
    Profile profile;

    int i = 0;
    String destinationImageName;


    int tooltipColor,tooltipSize,tipSizeSmall,tipSizeRegular,tipRadius;
    ListView listView;
    Tooltip tooltip;

    File downloadFolder;
    File downloadedImageFiles[] = null;

    boolean isPresent = false;

    private BroadcastReceiver broadcastReceiver;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        tooltipProperties();

        progressBar = (AVLoadingIndicatorView) findViewById(R.id.activity_ar_progressBar);

        betaButton = (Button)findViewById(R.id.beta);
        betaButton.setOnClickListener(this);

        nextButton = (FloatingActionButton) findViewById(R.id.next);
        nextButton.setOnClickListener(this);

        previousButton = (FloatingActionButton) findViewById(R.id.previous);
        previousButton.setOnClickListener(this);

        shopIcon = (FloatingActionButton) findViewById(R.id.shopIcon);
        shopIcon.setOnClickListener(this);

        infoIcon = (FloatingActionButton) findViewById(R.id.infoIcon);
        infoIcon.setOnClickListener(this);

        hamIcon = (FloatingActionButton) findViewById(R.id.hamIcon);
        hamIcon.setOnClickListener(this);

        logo = (ImageView)findViewById(R.id.ar_screen_logo);
        logo.setOnClickListener(this);

        root = (ViewGroup) findViewById(R.id.mainLayout);

        if(isTablet)
        {
            shopIcon.setSize(FloatingActionButton.SIZE_NORMAL);
            infoIcon.setSize(FloatingActionButton.SIZE_NORMAL);
            hamIcon.setSize(FloatingActionButton.SIZE_NORMAL);
        }else{
            shopIcon.setSize(FloatingActionButton.SIZE_MINI);
            infoIcon.setSize(FloatingActionButton.SIZE_MINI);
            hamIcon.setSize(FloatingActionButton.SIZE_MINI);
        }

        if(ViewUtils.isOnline(this))
        {
            apiCall();
        }else {
            NetworkError.show(this);
            progressBar.setVisibility(View.GONE);
        }

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
               if(intent.getAction().equals(ViewUtils.MESSAGE_RECEIVED)){
                   String message = intent.getStringExtra(ViewUtils.MESSAGE);
                   Messages.show(root,message);
                   NotificationDialog.show(Dashboard.this,message);
               }
            }
        };

    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,
                new IntentFilter(ViewUtils.MESSAGE_RECEIVED));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        super.onPause();
    }



    public void apiCall()
    {
        profileCall = apiService.getUserProfile(authenticate);
        profileCall.enqueue(profileCallback);
    }

    public void reApiCall()
    {
        progressBar.setVisibility(View.VISIBLE);
        apiCall();
    }

    @Override
    public void onClick(View view) {
        if (view == nextButton) {
            if (progressBar.getVisibility() == View.GONE) {

                if(profile == null ||profile.getData().getArlist() == null || profile.getData().getArlist().size() == 0)
                {
                    NoArtWork.show(this);
                    return;
                }

                i++;
                if (i < profile.getData().getArlist().size()) {

                    downloadFolder = ViewUtils.getDirectory(Dashboard.this);
                    if (downloadFolder != null) {
                        downloadedImageFiles = downloadFolder.listFiles();
                        for (File downloadedImageFile : downloadedImageFiles) {
                            String downloadedImageName = downloadedImageFile.getName();
                            String imageName = profile.getData().getArlist().get(i).getName() + ".png";
                            if (downloadedImageName.equals(imageName)) {
                                isPresent = true;
                                break;
                            }
                        }
                    }

                    if(isPresent) {
                        isPresent = false;
                        hideAll();
                        trackable.getWorld().getChildren().get(i).setVisible(true);
                    }else{
                        progressBar.setVisibility(View.VISIBLE);
                        requestImage();
                    }


                } else {
                    i = 0;
                    hideAll();
                    trackable.getWorld().getChildren().get(i).setVisible(true);
                }
            }

        } else if (view == previousButton) {
            if (progressBar.getVisibility() == View.GONE) {

                if(profile == null ||profile.getData().getArlist() == null || profile.getData().getArlist().size() == 0)
                {
                    NoArtWork.show(this);
                    return;
                }

                i--;
                if (i < 0) {
                    i = 0;
                    hideAll();
                    trackable.getWorld().getChildren().get(i).setVisible(true);
                } else {
                    hideAll();
                    trackable.getWorld().getChildren().get(i).setVisible(true);
                }

            }
        }  else if (view == shopIcon) {
            if (progressBar.getVisibility() == View.GONE) {
                showTooltip(view, shopIconList, Tooltip.TOP, true,
                        TooltipAnimation.REVEAL, 320,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
            }

        } else if (view == infoIcon) {
            if (progressBar.getVisibility() == View.GONE) {
                showTooltip(view, infoIconList, Tooltip.TOP, true,
                        TooltipAnimation.REVEAL, 360,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

            }


        } else if (view == hamIcon) {
            if (progressBar.getVisibility() == View.GONE) {
                showTooltip(view, hamIconList, Tooltip.TOP, true,
                        TooltipAnimation.REVEAL, 250,
                        ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        }else if(view == betaButton)
        {
            if(progressBar.getVisibility() == View.GONE)
            {
                Feedback.show(this,userName,trelloService);
            }
        }else if(view == logo)
            ViewUtils.openBrowser(this,getString(R.string.AddictedGallery));

    }

    private Callback<Profile> profileCallback = new Callback<Profile>() {
        @Override
        public void onResponse(Call<Profile> call, Response<Profile> response) {
            if (response.isSuccessful()) {
                profile = response.body();

                System.out.println("Registration Token" +preferences.getRegistrationToken());


                if(!preferences.isAlreadySubscribed())
                {
                    if(preferences.getRegistrationToken() != null)
                    {
                        SNSPush snsPush = new SNSPush();
                        Container container = new Container();
                        container.profileData = profile.getData();
                        container.preferences = preferences;
                        container.token = preferences.getRegistrationToken();
                        snsPush.execute(container);
                    }
                }
                else
                {
                    System.out.println("Arn:"+preferences.getEndpointArn());
                }

                if (profile.getStatus()) {

                    if(profile.getData().getArlist() != null && profile.getData().getArlist().size() != 0) {

                        final String imageUrl = profile.getData().getArlist().get(i).getImage();
                        String my_new_str = imageUrl.replaceAll("large", "shadow");
                        String finalImageUrl = my_new_str.replaceAll("jpg", "png");
                        final String imageName = profile.getData().getArlist().get(i).getName();

                        destinationImageName = getExternalFilesDir(null) + File.separator + imageName + ".png";

                        Ion.with(Dashboard.this)
                                .load(finalImageUrl)
                                .write(new File(destinationImageName))
                                .setCallback(new FutureCallback<File>() {
                                    @Override
                                    public void onCompleted(Exception e, File file) {

                                        if (file != null) {
                                            addImageNode(file.getName());
                                            progressBar.setVisibility(View.GONE);
                                        } else {
                                            progressBar.setVisibility(View.GONE);
                                            NetworkError.show(Dashboard.this);
                                        }

                                    }
                                });

                    }
                    else{
                        progressBar.setVisibility(View.GONE);
                         NoArtWork.show(Dashboard.this);
                    }


                }
                else {
                    progressBar.setVisibility(View.GONE);
                    preferences.clearData();
                    ThreadExecution.startScreen(Dashboard.this, Login.class);
                }

            }else{
                preferences.clearData();
                ThreadExecution.startScreen(Dashboard.this, Login.class);
            }
        }

        @Override
        public void onFailure(Call<Profile> call, Throwable t) {
            progressBar.setVisibility(View.GONE);
            NetworkError.show(Dashboard.this);
        }
    };






    private void addImageNode(String imageName) {
        String destination = getExternalFilesDir(null) + File.separator + imageName;
        ARTexture2D texture = new ARTexture2D();
        texture.loadFromPath(destination);
        texture.setTextureID(i);
        ARImageNode imageNode = new ARImageNode(texture);

        String width = profile.getData().getArlist().get(i).getWidth();
        String height = profile.getData().getArlist().get(i).getHeight();
        float floatWidth = Float.parseFloat(width);
        float floatHeight = Float.parseFloat(height);

        if (floatWidth > floatHeight) {
            double scale = 1754 / (floatWidth * 3.78);
            scale = 1 / scale;
            scale = scale / 1.495728;
            imageNode.scaleByUniform((float) scale);

        } else if (floatHeight > floatWidth) {
            double scale = 1754 / (floatHeight * 3.78);
            scale = 1 / scale;
            scale = scale / 1.495728;
            imageNode.scaleByUniform((float) scale);
        } else {
            double scale = 1754 / (floatWidth * 3.78);
            scale = 1 / scale;
            scale = scale / 1.495728;
            imageNode.scaleByUniform((float) scale);
        }

        trackable.getWorld().addChild(imageNode);


    }

    @Override
    public void didDetect(ARImageTrackable arImageTrackable) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                nextButton.setVisibility(View.VISIBLE);
                previousButton.setVisibility(View.VISIBLE);
            }
        });

    }

    @Override
    public void didTrack(ARImageTrackable arImageTrackable) {

    }

    @Override
    public void didLose(ARImageTrackable arImageTrackable) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                nextButton.setVisibility(View.GONE);
                previousButton.setVisibility(View.GONE);
            }
        });
    }


    private void requestImage() {
        final String imageUrl = profile.getData().getArlist().get(i).getImage();
        String my_new_str = imageUrl.replaceAll("large", "shadow");
        String finalImageUrl = my_new_str.replaceAll("jpg","png");
        final String imageName = profile.getData().getArlist().get(i).getName();

        destinationImageName = getExternalFilesDir(null) + File.separator + imageName + ".png";

        Ion.with(Dashboard.this)
                .load(finalImageUrl)
                .write(new File(destinationImageName))
                .setCallback(new FutureCallback<File>() {
                    @Override
                    public void onCompleted(Exception e, File file) {

                        if (file != null) {
                            addImageNode(file.getName());
                            progressBar.setVisibility(View.GONE);
                            hideAll();
                            trackable.getWorld().getChildren().get(i).setVisible(true);
                        } else {
                            progressBar.setVisibility(View.GONE);
                            NetworkError.show(Dashboard.this);
                            i--;
                        }

                    }
                });

    }

    private void hideAll() {
        List<ARNode> nodes = trackable.getWorld().getChildren();
        for (ARNode node : nodes) {
            node.setVisible(false);
        }
    }

    private void tooltipProperties() {
        Resources res = getResources();
        tooltipSize = res.getDimensionPixelOffset(R.dimen.tooltip_width);
        tooltipColor = ContextCompat.getColor(this, R.color.pink);
        tipSizeSmall = res.getDimensionPixelSize(R.dimen.tip_dimen_small);
        tipSizeRegular = res.getDimensionPixelSize(R.dimen.tip_dimen_regular);
        tipRadius = res.getDimensionPixelOffset(R.dimen.tip_radius);
    }

    @SuppressLint("InflateParams")
    private void showTooltip(@NonNull View anchor, @NonNull ArrayList<String> iconList,
                             @Tooltip.Position int position, boolean autoAdjust,
                             @TooltipAnimation.Type int type,
                             int width, int height) {

        listView = (ListView) getLayoutInflater().inflate(R.layout.tooltip_listview, null);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.tooltip_list_item,R.id.tv ,iconList);
        listView.setAdapter(adapter);
        listView.setLayoutParams(new ViewGroup.LayoutParams(width, height));
        listView.setOnItemClickListener(this);
        showTooltip(anchor, listView, position, autoAdjust, type, tooltipColor);


    }

    private void showTooltip(@NonNull View anchor, @NonNull View content,
                             @Tooltip.Position int position, boolean autoAdjust,
                             @TooltipAnimation.Type int type,
                             int tipColor) {

        if(tooltip !=null)
            tooltip.dismiss(true);

        tooltip = new Tooltip.Builder(this)
                .anchor(anchor, position)
                .animate(new TooltipAnimation(type, 500))
                .autoAdjust(autoAdjust)
                .content(content)
                .withTip(new Tooltip.Tip(tipSizeRegular, tipSizeRegular, tipColor))
                .into(root)
                .autoCancel(3000)
                .debug(true)
                .show();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        String listItem = (String) listView.getItemAtPosition(position);

        if (tooltip != null)
            tooltip.dismiss(true);

        switch (listItem) {

            case "ABOUT US":
                ViewUtils.openBrowser(this,getString(R.string.About));
                break;

            case "TUTORIAL":
                ThreadExecution.start(this, Guide.class,getString(R.string.Intent));
                break;

            case "LOG OUT":
                LogOut.show(this,preferences);
                break;

            case "REPORT ISSUE":
                Feedback.show(this,userName,trelloService);
                break;



            case "ABOUT THIS ARTWORK":

                if(profile == null ||profile.getData().getArlist() == null || profile.getData().getArlist().size() == 0)
                {
                    NoArtWork.show(this);
                    return;
                }

                for (int j = 0; j < profile.getData().getArlist().size(); j++) {
                    if (trackable.getWorld().getChildren().get(j).getVisible()) {
                        ArtWork.show(Dashboard.this,profile.getData().getArlist().get(j));
                        break;
                    }
                }
                break;


            case "CHECK AVAILABILITY":

                if(profile == null || profile.getData().getArlist() == null || profile.getData().getArlist().size() == 0)
                {
                    NoArtWork.show(this);
                    return;
                }

                for (int j = 0; j < profile.getData().getArlist().size(); j++) {
                    if (trackable.getWorld().getChildren().get(j).getVisible()) {
                        ViewUtils.openBrowser(this, getString(R.string.BuyArt) + profile.getData().getArlist().get(j).getPage());
                        break;
                    }
                }

                break;

            case "CONTACT US":
                ViewUtils.openBrowser(this,getString(R.string.Contact));
                break;








        }
    }
}
