package com.addictedartgallery;


import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;

import com.addictedartgallery.rest.ApiClient;
import com.addictedartgallery.rest.ApiInterface;
import com.addictedartgallery.rest.TrelloClient;
import com.addictedartgallery.rest.TrelloInterface;
import com.addictedartgallery.utils.Preferences;
import com.addictedartgallery.utils.ThreadExecution;
import com.addictedartgallery.utils.ViewUtils;
import com.addictedartgallery.views.activities.Login;

import java.util.ArrayList;

import eu.kudan.kudan.ARAPIKey;
import eu.kudan.kudan.ARActivity;
import eu.kudan.kudan.ARImageTrackable;
import eu.kudan.kudan.ARImageTrackableListener;
import eu.kudan.kudan.ARImageTracker;

public abstract class BaseDashboardActivity extends ARActivity implements ARImageTrackableListener {

    protected boolean isTablet;
    protected ArrayList<String> hamIconList,shopIconList,infoIconList;
    protected ApiInterface apiService;
    protected TrelloInterface trelloService;
    protected Preferences preferences;

    protected String userName,realm,accessToken;

    protected String authenticate;

    protected ARImageTrackable trackable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        ARAPIKey key = ARAPIKey.getInstance();
        key.setAPIKey("hcMGXUL7ifKYir30RzzGbtOG41is8NzxiGOnhVzp5k0rjsarMNqGhWM8gcfP8S2ls/shUpYmE0HtWr+UA3J77oPT0OCRQzNBSbADHDqMo0utNlYy4m8bbHltd8SkgTA3d+wRByGjJQw++j1iSlOSAbz/SglpyIshnCJg4bwS41DNhBap7vrPmh0gi6HlVFntowu0NwfcedK56i2zlfpu7Mnt0BvGE6KEny4nzA/eKAvxwYLD3rIKJ2TyLEcXGTuTQjk1lQ9OWjCihk6SNpzVHSFpUJPPv1HZTYApleEYfoPGhbpNH8PPZ/9qrsXVqeUfcL/VOPMU16DCzgRapcPoZ+LMxVqhYIZxnf/AiLWVuXmvZDf3nqSQ8dLZGWNZ3kXfxre4cGDV7yF3s1oKwj4bR9JgJ8k3S+4/OPJRrr9USE0L1gmvehsXdiafqxSSExJiJLrZTetLmie4063K3cH19ho6+933AxwOsqdcU+38C6BJ+xBqJi4AwJiGDzqrKEBuFMi24fSa8Ac3vkZQi6WYfMS//wdRlCQ2Mu34Eoc2kWs6hdhhyVmnOJXcDWJbzwFrFITr615A3LrQOfBc3InPJzvmkLYt7SFquaUsFapyy6VY4xm9c3/ggCc7OZFPLyq0uS4aKP3iyyRmiLxIhGvsl6zrnQ9njx9yLKYHPEIZA74=");
        isTablet = isTablet();

        hamIconList = new ArrayList<>();
        hamIconList.add(getString(R.string.AboutUs));
        hamIconList.add(getString(R.string.Tutorial));
        hamIconList.add(getString(R.string.LogOut));
        hamIconList.add(getString(R.string.ReportIssue));

        infoIconList = new ArrayList<>();
        infoIconList.add(getString(R.string.AboutThisArtwork));

        shopIconList = new ArrayList<>();
        shopIconList.add(getString(R.string.BuyArtWork));
        shopIconList.add(getString(R.string.ContactUs));



        apiService = ApiClient.getClient().create(ApiInterface.class);
        trelloService = TrelloClient.getClient().create(TrelloInterface.class);

        preferences = new Preferences(this);

        userName = preferences.getUsername();
        realm = preferences.getRealm();
        accessToken = preferences.getAccessToken();

        if(userName != null && realm != null && accessToken != null)
        {
            String r1 = ViewUtils.getMd5Key(userName + ":" + realm + ":" + accessToken);
            String r2 = ViewUtils.getMd5Key("GET:https://devapi.addictedgallery.com/en/account/profile:");

            String auth = ViewUtils.getMd5Key(r1 + ":" + accessToken + ":" + r2);
            authenticate = userName + ":" + auth;
        }else
        {
            preferences.clearData();
            ThreadExecution.startScreen(this,Login.class);
        }

    }

    private boolean isTablet()
    {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        return (metrics.widthPixels /metrics.density) >=600;
    }

    @Override
    public void setup() {
        super.setup();
        addImageTrackable();
    }

    protected void addImageTrackable() {
        trackable = new ARImageTrackable("marker");
        trackable.loadFromAsset("addictedmarker.jpg",true);
        trackable.setExtensible(true);
        trackable.addListener(this);
        ARImageTracker trackableManager = ARImageTracker.getInstance();
        trackableManager.addTrackable(trackable);

    }

    @Override
    public void didDetect(ARImageTrackable arImageTrackable) {

    }

    @Override
    public void didTrack(ARImageTrackable arImageTrackable) {

    }

    @Override
    public void didLose(ARImageTrackable arImageTrackable) {

    }
}
